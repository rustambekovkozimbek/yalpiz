import messages from "./locale/index.js";
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "yalpiz",
    htmlAttrs: {
      lang: "en",
    },
    meta: [{
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: ""
      },
      {
        name: "format-detection",
        content: "telephone=no"
      },
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "/favicon.ico"
    }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/css/style.css", "~/assets/fonts/stylesheet.css", ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vv-mask.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    "bootstrap-vue/nuxt",
    '@nuxtjs/i18n',
  ],
  i18n: {
    locales: [{
        code: "uz",
        name: "O'zbek"
      },
      {
        code: "ru",
        name: "Русский"
      },
      {
        code: "en",
        name: "English"
      }
    ],
    strategy: "prefix_except_default",
    defaultLocale: "ru",
    vueI18n: {
      fallBackLocale: "ru",
      messages: messages
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
