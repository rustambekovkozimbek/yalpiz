import uz from "./uz";
import ru from "./ru";
import en from "./en";

const messages = {
  uz,
  ru,
  en,
};

export default messages;